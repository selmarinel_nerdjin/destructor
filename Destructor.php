<?php

namespace Destructor;

use Destructor\App\Captcha\CaptchaInterface;
use Destructor\App\Connectors\ConnectInterface;
use Destructor\App\Encrypts\EncryptInterface;

class Destructor
{
    /**
     * @var CaptchaInterface
     */
    private $captcha;
    /**
     * @var ConnectInterface
     */
    private $connector;
    /**
     * @var EncryptInterface
     */
    private $encrypt;

    const DESTRUCTOR_TYPE_TIME = 1;
    const DESTRUCTOR_TYPE_CLICK = 2;

    public function __construct(
        ConnectInterface $connect,
        CaptchaInterface $captcha,
        EncryptInterface $encrypt
    )
    {
        $this->connector = $connect;
        $this->captcha = $captcha;
        $this->encrypt = $encrypt;
    }

    public function setMessage(Array $options)
    {
        $message = ($options["message"]) ?: "";
        $password = ($options["password"]) ?: "";
        $type = ($options["type"])? : self::DESTRUCTOR_TYPE_TIME;

        $this->connector->connect();
        $key = $this->captcha->generate();
        $message = $this->encrypt->encrypt($message, $key);

        try {
            $this->connector->set($key, $message, $password, $type);
            $this->connector->close();
            return $key;
        } catch (\RedisException $redisException) {
            throw $redisException;
        }
    }

    public function getMessage($key, $password = "")
    {
        $this->connector->connect();
        try {
            $data = $this->connector->getByKey($key, $password);
            $message = $data["message"];
            /**
             * Todo modify
             */
            if ($data["type"] == self::DESTRUCTOR_TYPE_CLICK) {
                $this->connector->deleteByKey($key);
            }
            $this->connector->close();
            return $this->encrypt->decrypt($message, $key);
        } catch (\RedisException $redisException) {
            throw $redisException;
        }
    }

}