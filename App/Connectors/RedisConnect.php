<?php

namespace Destructor\App\Connectors;

class RedisConnect implements ConnectInterface
{
    protected $connect;

    protected $host;
    protected $port;
    protected $timeout;

    public function __construct()
    {
        $this->connect = new \Redis();
        $this->host = getenv('REDIS_HOST');
        $this->port = (int)getenv('REDIS_PORT');
        $this->timeout = (int)getenv('REDIS_TIMEOUT');
    }

    /**
     * @void
     */
    public function connect()
    {
        $this->connect->connect($this->host, $this->port, $this->timeout);
    }

    /**
     * @param $key
     * @param $data
     * @param string $salt
     * @param string $type
     * @throws \RedisException
     */
    public function set($key, $data, $salt = "", $type = "")
    {
        try {
            $this->connect->hMSet($key, ['message' => $data, 'password' => md5($salt), "type" => $type]);
            /**
             * todo: modify
             */
            if ($type == 1) {
                $this->connect->setTimeout($key, $this->timeout);
            }
        } catch (\RedisException $exception) {
            throw $exception;
        }
    }

    /**
     * @param $key
     * @param string $password
     * @return mixed
     * @throws \RedisException
     */
    public function getByKey($key, $password = "")
    {
        try {
            if ($this->connect->exists($key)) {
                $connect = $this->connect->hMGet($key, ["message", "password"]);
                if ($connect["password"] == md5($password)) {
                    return [
                        "message" => $connect["message"],
                        "type" => $connect["type"]
                    ];
                }
                throw new \RedisException("Incorrect Password", 403);
            }
            throw new \RedisException("Invalid Key", 404);
        } catch (\RedisException $exception) {
            throw $exception;
        }
    }

    /**
     * @param $key
     * @return bool
     * @throws \RedisException
     */
    public function deleteByKey($key)
    {
        try {
            if ($this->connect->hExists($key, "message")) {
                $this->connect->delete($key);
                return true;
            }
            throw new \RedisException("Invalid Key", 404);
        } catch (\RedisException $exception) {
            throw $exception;
        }
    }

    public function close()
    {
        try {
            $this->connect->close();
        } catch (\RedisException $exception) {
            throw $exception;
        }
    }

}