<?php

namespace Destructor\App\Connectors;


interface ConnectInterface
{

    public function connect();

    public function close();

    public function getByKey($key, $password = "");

    public function set($key, $data, $salt = "", $type = "");

    public function deleteByKey($key);
}