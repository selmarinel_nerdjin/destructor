<?php
/**
 * Created by PhpStorm.
 * User: Selmarinel
 * Date: 18.04.2017
 * Time: 19:22
 */

namespace Destructor\App\Captcha;


class ShaCaptcha implements CaptchaInterface
{
    public function generate()
    {
        return sha1(time());
    }
}