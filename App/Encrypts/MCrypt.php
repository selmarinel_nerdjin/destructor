<?php
/**
 * Created by PhpStorm.
 * User: Selmarinel
 * Date: 18.04.2017
 * Time: 20:59
 */

namespace Destructor\App\Encrypts;


class MCrypt implements EncryptInterface
{
    const SECRET_KEY = "secret key";
    const SECRET_IV = "secret iv";

    private $encrypt_method = "AES-256-CBC";

    private $key;

    public function __construct()
    {
        $this->key = getenv("SECRET_KEY") || self::SECRET_KEY;
    }

    public function encrypt($encrypt, $base64 = true, $salt = "")
    {
        $salt = $salt || $this->key;

        $key = hash('sha256', $salt);
        $iv = substr(hash('sha256', self::SECRET_IV), 0, 16);
        $output = openssl_encrypt($encrypt, $this->encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
        return $output;
    }

    public function decrypt($decrypt, $base64 = true, $salt = "")
    {
        $salt = $salt || $this->key;

        $key = hash('sha256', $salt);
        $iv = substr(hash('sha256', self::SECRET_IV), 0, 16);

        $output = openssl_decrypt(base64_decode($decrypt), $this->encrypt_method, $key, 0, $iv);
        return $output;

    }
}