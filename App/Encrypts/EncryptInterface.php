<?php
/**
 * Created by PhpStorm.
 * User: Selmarinel
 * Date: 18.04.2017
 * Time: 20:21
 */

namespace Destructor\App\Encrypts;


interface EncryptInterface
{
    public function encrypt($encrypt, $base64 = true, $salt = "");

    public function decrypt($decrypt, $base64 = true, $salt = "");
}